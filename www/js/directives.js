angular.module('starter.directives', [])
.directive('fileModel', ['$parse', function ($parse) {

            return {
               restrict: 'A',
               link: function(scope, element, attrs,ngMode) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  var validFormats = ['jpg', 'gif'];
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                        var value = element.val(),
                        ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();   
 
                    return validFormats.indexOf(ext) !== -1;
 
                     });
                  });
               }
            };
         }])
         