// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova','ion-autocomplete','ion-select-autocomplete','starter.directives','angularUtils.directives.dirPagination'])

.run(function($ionicPlatform,$cordovaSQLite,$http) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      db = $cordovaSQLite.openDB( "my.db");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS countries (id integer primary key,code text,pays text,activation Integer)");
      if(db==null)
      {
      $http.get("http://services.groupkt.com/country/get/all").then(function(success){
       
        
        console.log(success.data.RestResponse.result)
        var x
        for( x in success.data.RestResponse.result){
         var query = "INSERT INTO countries (code,pays,activation) VALUES (?,?,?)";
         var codes=success.data.RestResponse.result[x].alpha2_code;
         if(codes=="FR"||codes=="MA"||codes=="TN"){

    
         $cordovaSQLite.execute(db, query, [success.data.RestResponse.result[x].alpha2_code,success.data.RestResponse.result[x].name,1]).then(function(res) {
          console.log("INSERT ID -> " +success.data.RestResponse.result[x].alpha2_code+" "+success.data.RestResponse.result[x].name);
         }, function (err) {
             console.error(err);
         });
        }
        else{
          $cordovaSQLite.execute(db, query, [success.data.RestResponse.result[x].alpha2_code,success.data.RestResponse.result[x].name,0]).then(function(res) {
            console.log("INSERT ID -> " +success.data.RestResponse.result[x].alpha2_code+" "+success.data.RestResponse.result[x].name);
           }, function (err) {
               console.error(err);
           });
        }
        /*var query = "SELECT firstname, lastname FROM people WHERE lastname = ?";
         $cordovaSQLite.execute(db, query, ['AF']).then(function(res) {
             if(res.rows.length > 0) {
                 console.log("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname+" sieze "+res.rows.item.length);
             } else {
                 console.log("No results found");
             }
         }, function (err) {
             console.error(err);
         });*/
        }
      }),function(error){
        var tabaleau=[{"pays":"france","code":"fr","activation":1},{"pays":"maroc","code":"ma","activation":1},
       {"pays":"belgique","code":"be","activation":1},{"pays":"tunise","code":"tu","activation":1},
       {"pays":"suisse","code":"su","activation":1},{"pays":"algerie","code":"al","activation":1}]
       for(x in tabaleau ){
        var query = "INSERT INTO countries (code,pays,activation) VALUES (?,?,?)";
        $cordovaSQLite.execute(db, query, [x.code,x.pays,0]).then(function(res) {
            console.log("INSERT ID -> " + x.pays+" "+x.code);
        }, function (err) {
            console.error(err);
        })};
    }
  }
    } else{
       db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100);
       $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS countries (id integer primary key,code text,pays text,activation Integer)");
       // browser
       
       console.log("browser");
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('login', {
    url: '/login',

    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
})
.state('acount', {
  url: '/acount',
  templateUrl: 'templates/testcompte.html',
  controller: 'LoginCtrl'
})
.state('app', {
  url: '/app',
  //abstract: true,
  templateUrl: 'templates/menu.html',
  controller: 'menu'
}).state('app.home',{
  url:'/home',
    views:{
      'menuContent': {
  templateUrl:'templates/home.html',
  controller:'home'
  
  }}
    }).state('app.profil',{
      url:'/profil',
        views:{
          'menuContent': {
      templateUrl:'templates/profiletest.html',
      controller:'profil'
      
      }}
        }).state('app.editeprofil',{
          url:'/editionprofile',
          views: {
            'menuContent':{
              templateUrl:'templates/chat-detail.html',
              controller:'editprofil'
            }
          }
        })
        
        .state('app.profiltest',{
              url:'/proftest',
                views:{
                  'menuContent': {
              templateUrl:'templates/profiletest.html',
              controller:'profil'
              
              }}
                }).state('app.dash',{
                  url:'/dash',
                    views:{
                      'menuContent': {
                  templateUrl:'templates/tab-dash.html',
                  controller:'Crltconge'
                  
                  }}
                    }).state('app.note',{
                      url:'/note',
                        views:{
                          'menuContent': {
                      templateUrl:'templates/tab-chats.html',
                      controller:'NoteCrtl'
                      
                      }}
                        }).state('app.profilenote',{
                          url:'/profilenote',
                            views:{
                              'menuContent': {
                          templateUrl:'templates/noteprofil.html',
                          controller:'ConsuNoteCrtl'
                          
                          }}
                            }).state('app.consultationNote',{
                          url:'/consultationNote',
                            views:{
                              'menuContent': {
                          templateUrl:'templates/consunote.html',
                          controller:'ConsuNoteCrtl'
                          
                          }}
                            })
                        .state('app.consult',{
                          url:'/consultation',
                            views:{
                              'menuContent': {
                          templateUrl:'templates/consulation.html',
                          controller:'consulCrtl'
                          
                          }}
                            }).state('app.profilselect',{
                              url:'/consultation/profil',
                                views:{
                                  'menuContent': {
                              templateUrl:'templates/profilselect.html',
                              controller:'profselectCrtl'
                              
                              }}
                                }).state('app.creation',{
                                  url:'/creation',
                                    views:{
                                      'menuContent': {
                                  templateUrl:'templates/creation.html',
                                  controller:'CreationCrlt'
                                  
                                  }}
                                    }).state('app.remarquenote',{
                                      url:'/remarquenote',
                                        views:{
                                          'menuContent': {
                                      templateUrl:'templates/remarque.html',
                                      controller:'ConsuNoteCrtl'
                                      
                                      }}
                                        }).state('app.editeNote',{
                                          url:'/editeNote',
                                            views:{
                                              'menuContent': {
                                          templateUrl:'templates/editenote.html',
                                          controller:'editeNoteCrtl'
                                          
                                          }}
                                            });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise(function($injector, $location){
    var state = $injector.get('$state');
    console.log("user_id",window.localStorage.getItem("user_id"));
    if(window.localStorage.getItem("user_id")!=null)
      state.go('app.home');

    else
      state.go('login');
   return $location.path();
 });

});
