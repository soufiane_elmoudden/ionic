angular.module('starter.services', [])

.factory('LoginService', function($http, $q) {
	return {
            loginUser: function(email, pw) {

		var deferred = $q.defer();
		var promise = deferred.promise;
		
		var link = 'http://79.137.38.63/users/login.json';
		//var link = 'http://lab.aplicacionescolar.com/users/signin';
		var config = {
                    headers : {'Access-Control-Allow-Origin' : '*',
				'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Headers':'X-Requested-With',	
			'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }


		}
		
		$http.post(link,{User:{'password':pw,'login':email}}).then(function(response){
		    //console.info(response);
		    deferred.resolve(response.data);
		}, function(errResponse){
		    deferred.reject(errResponse);
		});

		return promise;
            }			
	}
    })
    .factory('NewAcountservice', function($http, $q){
      return {
                  signupUser: function(email,pw,firstname,lastname,role,user_id) {
      
          var deferred = $q.defer();
          var promise = deferred.promise;
          
          
          var link = 'http://localhost:8080/newapp/users/add.json';
          
          var config = {
                          headers : {
                  'Access-Control-Allow-Origin' : '*',
              'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
              'Content-Type': 'application/json',
              'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                          }
          }
      
          $http.post(link, {User:{'password': pw,'nom':firstname,'prenom':lastname,'login':email}}).
          then(function(response){
          
              deferred.resolve(response.data);
          }, function(errResponse){
              deferred.reject(errResponse);
          });
      
          return promise;
                  }}
      }).factory('weekend',function()
      {
       return {
                  comptweekend: function(d1,d2) {
         var numberweekend=0;
              while (d1 <= d2) {
              var day = d1.getDay();
              if ((day === 6) || (day === 0)) { 
                  
                      numberweekend++; // if next day is also a weekend, return true
          
              
                      console.log(numberweekend);
          }
              d1.setDate(d1.getDate() + 1);
          }
              return numberweekend;
                  }}   
      })
      