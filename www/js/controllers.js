angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope,$window,$location,$http,LoginService,NewAcountservice ,$ionicPopup, $state, $ionicLoading, $ionicHistory){
  
    
  $scope.User={}
    //console.log('soufiane');
  /*  if(window.localStorage.getItem("user_id"))
      {
        $state.go('home')
      }*/
    $scope.login = function(){
      
        window.localStorage.clear();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
       
      if($scope.User.username==null||$scope.User.Password==null)
    {
      $scope.User= {};
      var alertPopup = $ionicPopup.alert({
        title: ' erreur',
        template: 'email ou passwod sont null.'
          });
    } 
    else{
        $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <br/> Charger les données.'
        });
        LoginService.loginUser($scope.User.username, $scope.User.Password).then(function(data){
      $ionicLoading.hide();
      $scope.User=null;
      console.log(JSON.stringify(data));
      if(data.message.status==200&&data.message.success==1){
  
          var names = data.user.prenom+" "+data.user.nom
        var role=data.user.email;
         var id =data.user.id;
          
          window.localStorage.setItem("user_session", names);
        //Session.get('user',id)
        window.localStorage.setItem("user_nom",data.user.nom)
        window.localStorage.setItem("user_prenom",data.user.prenom)
         window.localStorage.setItem("user_email", role);
         window.localStorage.setItem("user_ville",data.user.ville);
         window.localStorage.setItem("user_emalp",data.user.email_quanteam);
        window.localStorage.setItem("user_date",data.user.date_creation)
        window.localStorage.setItem("user_ne",data.user.ne_le)
        window.localStorage.setItem("civil",data.user.etat_civil)
        window.localStorage.setItem("user_pays",data.user.etat_pays_nais)
        window.localStorage.setItem("user_role",data.user.Role.role)
        window.localStorage.setItem("user_entite",data.user.EntiteInterne.raison_social)
        window.localStorage.setItem("user_fille",data.user.jeune_fille)
        window.localStorage.setItem("user_adressc",data.user.comp_adresse)
        window.localStorage.setItem("user_adress",data.user.adresse)
        window.localStorage.setItem("user_post",data.user.code_post)
        window.localStorage.setItem("user_tel",data.user.tel)
        window.localStorage.setItem("user_natio",data.user.natio)
          window.localStorage.setItem("user_id", id);
         
        $state.go('app.home');
       
    
  
           
      }	
      if(data.message.status==200&&data.message.success==0){
        $scope.User= {};
        var alertPopup = $ionicPopup.alert({
        title: 'Votre e-mail ou votre mot de passe est incorrect!',
        template: 'vérifiez vos informations.'
          });
         
      }	
        },function(err){
          $scope.User= {};
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
        title: "Erreur!",
        template: 'vérifiez votre connexion et réessayez.'
          });
        });
    }
  }
  $scope.doCreateAccountAction=function(){
		alert("vous voulez créer un compte");
    setTimeout(function() {
      $state.go('acount');
    }, 1000);
  };
 $scope.account={};
 //var link = 'http://localhost:8080/ionic_cake/users/add.json';
  $scope.signup=function(){
	
	    $ionicHistory.clearCache();
	    $ionicHistory.clearHistory();
		if($scope.account.email!=null&&$scope.account.PassWord!=null&&$scope.account.fisrtnames!=null&&$scope.account.lastnames!=null)
		{
	    $ionicLoading.show({
    template: '<ion-spinner></ion-spinner> <br/> envoi de données...'
	    });
	    NewAcountservice.signupUser($scope.account.email, $scope.account.PassWord,$scope.account.fisrtnames,$scope.account.lastnames).then(function(data){
		$ionicLoading.hide();
		console.log(JSON.stringify(data));
		if(data.message.type=='info'){
		    var names = $scope.account.fisrtnames+" "+$scope.account.lastnames;
		    
        window.localStorage.setItem("user_session", names);
        var alertPopup = $ionicPopup.alert({
          title: 'le candidat est registé ',
          template: 'bon.'
        
           });
           
		    
		}
     
        if(data.message.type=='error'){             
		    var alertPopup = $ionicPopup.alert({
			title: 'données non sauvegardée',
			template: 'réessayer.'
		
			 });}
		});}
		else{
			var alertPopup = $ionicPopup.alert({
			title: 'Erreur!',
			template: 'SVP insérer toutes les informations.'
			 });
		}
	};
	
	
    

}).controller('home',function($scope,$http,$ionicLoading,$ionicPopup){

}).controller('menu',function($scope,$state,$ionicLoading,$ionicPopup,$http,$ionicHistory){
  $scope.consultation=function(a){
    if(a==2)
      {
       $state.go("app.dash")
      }
      if(a==5)
        {
          $state.go('app.note')
        }
        if(a==6)
        {
          $state.go("app.consultationNote")
        }
  }
  $scope.consultationa=function(a){
    if(a==1)
      {
       $state.go("app.consult")
      }
      else
      {
        if(a==2)
        {
          $state.go("app.creation")
        }
      }

  }
$scope.profil=function(){
  $state.go('app.profil');
}
$scope.home=function()
{
  $state.go('app.home');
}
$scope.out=function(){
  $ionicLoading.show({
		template: '<ion-spinner></ion-spinner> <br/> logout....'
	    });
  $http.post('http://79.137.38.63/users/logout').then(function(success){
    $ionicLoading.hide();
 window.localStorage.clear();
  $ionicHistory.clearCache();
	    $ionicHistory.clearHistory();
		
				
$state.go('login');
  },function(err)
  {
		$ionicLoading.hide();
var popalert=$ionicPopup.alert({
  title:'à propos de la déconnexion',
  template:'désolé'
})
  })
}

}).controller('profil',function($scope,$http,$state,$ionicLoading,$ionicHistory,$ionicPopup
){

  $scope.info={
    'name':window.localStorage.getItem("user_session"),
    'email':window.localStorage.getItem("user_email"),
    'emailp':window.localStorage.getItem("user_emalp"),
     'ville':window.localStorage.getItem("user_ville"),
     'date':window.localStorage.getItem("user_date"),
     'nom':window.localStorage.getItem("user_nom"),
     'prenom':window.localStorage.getItem("user_prenom"),
    // 'born':window.localStorage.getItem("user_ne"),
    //'civil': window.localStorage.getItem("civil"),
     'country':window.localStorage.getItem("user_pays"),
     'role':window.localStorage.getItem("user_role"),
     'entite':window.localStorage.getItem("user_entite"),
     'girl':window.localStorage.getItem("user_fille"),
     'adressc':window.localStorage.getItem("user_adressc"),
     'adress':window.localStorage.getItem("user_adress"),
     'post':window.localStorage.getItem("user_post"),
     'tel':window.localStorage.getItem("user_tel"),
     'natio':window.localStorage.getItem("user_natio")
  
  };
  $scope.editeprofil=function(){
    $state.go('app.editeprofil')
  }
console.log("info:",$scope.info)

}).controller('Crltconge',function($scope,$http,$timeout,$state,weekend,$ionicHistory,$ionicPopup,$ionicLoading){
  $scope.aftervalide=true;
  $scope.notfirstime=false;
  $scope.sameday=true;
    var today=new Date();
  var init=new Date();
  init.setDate(init.getDate()-1);
 $scope.datedebut=init
  $scope.ajouter=true;
  $scope.send=true;
  $scope.beforevalide=false;
   var select=["paternitE","maternitE","congsans","cong"];

  $scope.mat=false;
  $scope.pat=false;
  $scope.conge=false;
  $scope.congesans=false
  $scope.chekdebutm=false;
 $scope.chekdebuts=false;
 $scope.chekfinm=false;
 $scope.chekfins=false;
$scope.dischekbox={
 debutmatin:false,
 debutsoire:false,
 finmatin:false,
 finsoire:false
}

 $scope.today = today.toISOString().split("T")[0];	
var dateselect
console.log('today:',today);
$scope.change=function(a)
{
var nextdate=new Date(a);
 nextdate.setDate(nextdate.getDate()+1);
 $scope.nextdate=nextdate.toISOString().split("T")[0];
 console.log('dateselect:',a);
 dateselect=a
}
$scope.changedatefin=function(a)
{
  var testdebut=new Date(dateselect)
  var testfin=new Date(a)
   testdebut.setHours(00)
   testdebut.setMinutes(00)
   testdebut.setSeconds(00)
   testfin.setHours(00)
   testfin.setMinutes(00)
   testfin.setSeconds(00)
   console.log("testdebut",testdebut)
   console.log("testfin",testfin)
  if(testdebut.getDate()==testfin.getDate())
    {
      $scope.sameday=false;
    }
    else{
      $scope.sameday=true; 
    }
}
$scope.changechekdm=function(a)
{
  
console.log('a',a);

if(a==true){
 $scope.dischekbox.debutsoire=true;
 console.log('datecheck:',dateselect)
 var next=new Date(dateselect);
 
 next.setMinutes(00);
 

 next.setDate(next.getDate());
 $scope.nextdate=next.toISOString().split("T")[0];
 
 
 console.log('dateselect:',a);
}

else
{
 $scope.dischekbox.debutsoire=false;
 var next=new Date(dateselect);
 next.setMinutes(00);
 next.setDate(next.getDate()+1);
 $scope.nextdate=next.toISOString().split("T")[0];
}
 
}
$scope.changechekds=function(b)
{
  
console.log('b',b);
if(b==true){
 $scope.dischekbox.debutmatin=true;
}

else
{
 $scope.dischekbox.debutmatin=false;
}
 
}
$scope.changechekfm=function(a)
{
  
console.log('a',a);

if(a==true){
  
 $scope.dischekbox.finsoire=true;
}

else
{
 $scope.dischekbox.finsoire=false;
}
 
}
$scope.changechekfs=function(b)
{
  
console.log('b',b);
if(b==true){
 $scope.dischekbox.finmatin=true;
}

else
{
 $scope.dischekbox.finmatin=false;
}
 
}
var typedemande=["1"];
var datedebut=["2"];
var statusdebut=["3"];
var datefin=["4"];
var statusfin= ["5"];
$scope.data = {
   model: null,
   
  };
  $scope.all=true;
 $scope.select=function()
 {
   if($scope.data.model=="")
   {
     $scope.all=true;
   }
   else
   {
       $scope.all=false;
   }
 }
 $scope.valide=function(a,b,c,d,e,f)
 {
   $scope.aftervalide=false;
  $scope.send=false;
  $scope.ajouter=false;
typedemande.push($scope.data.model);
 datedebut.push(a);
datefin.push(b);
 if(c==true)
 {
   statusdebut.push("matin");
 }
 if(d==true)
 {
   statusdebut.push("soire");
 }
 if(e==true)
 {
   statusfin.push("matin");
 }
 if(f==true)
 {
   statusfin.push("soire");
 }
 
 

   $scope.beforevalide=true;
console.log('tab1',typedemande);
 console.log('tab2',datedebut);
   console.log('tab2',datefin);
 var aj=1;

 $scope.ajoute=function(a,b){
   $scope.notfirstime=true;
  $scope.all=true;
  $scope.aftervalide=true
  aj++;
   console.log('tab2',$scope.data.model)
   if(select.indexOf($scope.data.model)==0)
   {
     $scope.pat=true;
     
   }
   if(select.indexOf($scope.data.model)==1)
   {
     $scope.mat=true;
       
   }
   
   
   //console.log("test",test);

 $scope.initdebut=new Date();
 //$scope.datefin="";
 console.log("pat",$scope.pat);
 console.log("mat", $scope.mat)
 console.log('datfin',$scope.data.model);
 $scope.data.model="";
 var date=new Date(b);
 date.setDate(date.getDate()+aj);
 var findate=new Date(b);
   findate.setDate(date.getDate()+2);
 $scope.today=date.toISOString().split("T")[0];
 $scope.nextdate=findate.toISOString().split("T")[0];
 console.log("dateajouti:",	$scope.nextdate);
 $scope.ajouter=true;
 $scope.send=true;
 $scope.beforevalide=false;
//	$window.location.reload();
 $scope.chekdebutm=false;
 $scope.chekdebuts=false;
 $scope.chekfinm=false;
 $scope.chekfins=false;
$scope.datedebut=new Date(b);
$scope.datefin="";

 setTimeout(function() {
   $scope.message = 'Fetched after two seconds';
   console.log('message:' + $scope.message);
   $scope.$apply(); //this triggers a $digest
 }, 3000);
 console.log("datebute",$scope.datedebut)

 //$scope.ajouter=true;
 //$scope.send=true;
 }
}
$scope.sende=function()
{
 
 var total=0;   //Math.round(new Date(datefin[1]).getTime()-new Date(datedebut[1]).getTime()/ (1000 * 3600 * 24))+1;

 var taile=typedemande.length;
 var i;
 
 var datedebuttostring;
 var datefintostring;
 var totalgroupe=["5"]
 for(i=1;i<taile;i++)
 {
   var numberweekend=0;
   var numholiday=0;
   var debut=new Date(datedebut[i])
   var fin=new Date(datefin[i])
   var debuth=new Date(datedebut[i])
   var finh=new Date(datefin[i])
   debut.setHours(00);
debut.setMinutes(00);
debut.setSeconds(00);

   console.log("finww",fin)
   console.log("debutww",debut);
   d1=new Date(datedebut[i]);
   d2=new Date(datefin[i]);
   while (d1 <= d2) {
       var day = d1.getDay();
       if ((day === 6) || (day === 0)) { 
           
               numberweekend++; // if next day is also a weekend, return true
   
       
       console.log('numberde weekend:',numberweekend);
   }
   d1.setDate(d1.getDate() + 1);
 }
 total=total+Math.round((fin.getTime()-debut.getTime())/ (1000 * 3600 * 24))-numberweekend;
 var link="https://www.googleapis.com/calendar/v3/calendars/uk__fr%40holiday.calendar.google.com/events?key=AIzaSyBHAAzRDCNe6oChUALUObPVzSFJM1qMyyA";
 $http.get(link).then(function(success){
   lengh=success.data.items.length;
   var j;
   
    console.log('josn lent:',lengh)
   for(j=0;j<lengh;j++)
   {
        var holidaydebutw=new Date(success.data.items[j].start.date);
        var holyidayfinw=new Date(success.data.items[j].end.date);
        var holyidayfin=new Date(success.data.items[j].end.date);
        var holidaydebut=new Date(success.data.items[j].start.date);
        finh.setHours(00);
        finh.setMinutes(00);
        finh.setSeconds(00);
        debuth.setHours(00);
        debuth.setMinutes(00);
        debuth.setSeconds(00);
        debuth.setMilliseconds(00);
        console.log('holiday debut:',holidaydebut);
        console.log('holiday fin:',holyidayfin)
        console.log('holiday fin:',finh);
        console.log('holiday debut:',debuth)
        console.log('getimedebut:',debuth.getTime());
        console.log('holidaydebut:',holidaydebut.getTime())
        console.log("getimefin",finh.getTime())
        console.log("holyidayfin",holyidayfin.getTime())
        
        
        var numholidayeteration=0;
        if(holyidayfin.getTime()<=debuth.getTime()||holidaydebut.getTime()>finh.getTime())
        {
             continue;
             console.log("i'm not yet in the date")
           /*numholiday=Math.round((holyidayfin.getTime()-holidaydebut.getTime())/ (1000 * 3600 * 24));
          numholiday=numholiday-weekend.comptweekend(holidaydebutw,holyidayfinw);
          console.log('nombre de hiliday:',holidaydebut);
          console.log('nombre de weeken:',holyidayfin)
        */
      
          
        }
        else
        {
          if(holidaydebut.getTime()<=debuth.getTime()&&holyidayfin.getTime()>=finh.getTime())
          {

             numholiday=Math.round((holyidayfin.getTime()-holidaydebut.getTime())/ (1000 * 3600 * 24));
          //numholiday=numholiday-weekend.comptweekend(holidaydebut,holyidayfinw);
          console.log('nombre de hiliday:',numholiday);
          console.log('ombre de weeken:',weekend.comptweekend(debuth,holyidayfinw))
          total=0;
          console.log('total is :',total)
          break;
         }
         else{
           if(holidaydebut.getTime()>=debuth.getTime()&&holyidayfin.getTime()<=finh.getTime())
           {
              numholiday=Math.round((holyidayfin.getTime()-holidaydebut.getTime())/ (1000 * 3600 * 24));
          total=total-weekend.comptweekend(holidaydebut,holyidayfin)-numholiday;
          
          console.log("i'm betwen date 1",total)
          console.log("diffrent",numholiday)
           }
           else
           {
             if(holidaydebut.getTime()>debuth.getTime()&&holyidayfin.getTime()>finh.getTime())
              {
                numholiday=Math.round((finh.getTime()-holidaydebut.getTime())/ (1000 * 3600 * 24));
                total=total-weekend.comptweekend(holidaydebut,finh)-numholiday;
                console.log("i'm betwen date 2",total);
              }
              else
                {
                 if(holidaydebut.getTime()<debuth.getTime()&&holyidayfin.getTime()<finh.getTime())
                  {
                    numholiday=Math.round((holyidayfin.getTime()-debuth.getTime())/ (1000 * 3600 * 24));

                    total=total-weekend.comptweekend(debuth,holyidayfin)-numholiday;
                  }
                }
           }
         }
        }
   }
 },function(erreur){
   
 })
 $timeout(function () {
 console.log("att")
 totalgroupe.push(total)
 console.log("totalfinal-:",total);
 console.log("totalfinal:",total)
 var url="http://localhost:8080/ionic_cake/demandes/add.json"
  $http.post(url,{'Demande':{'total':total}}).then(function(sucess){
    console.log('demamdes',sucess.data)
    console.log("totalfinal:",total)
    if(sucess.data.message.type=="info")
    {
    var allsave=1;
    
 for(i=1;i<taile;i++)
  {
    var debut=new Date(datedebut[i])
    var fin=new Date(datefin[i])
    console.log("fin",fin)
    console.log("debut",debut)
    fin.setHours(15);
 fin.setMinutes(00);
 fin.setSeconds(00);
  datedebuttostring=debut.toISOString().split("T")[0];
  
  datefintostring=fin.toISOString().split("T")[0];
  console.log("fin",datefintostring)
    console.log("debut",datedebuttostring)
  $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <br/> envoi demande numero'+i
    })
    window.localStorage.setItem('demande_id_email',sucess.data.message.damende_id)
  var url="http://localhost:8080/ionic_cake/groupedemandes/add.json"
  $http.post(url,{'GroupeDemande':{'demande_id':sucess.data.message.damende_id,'timedebut':statusdebut[i],'datedebut':datedebuttostring,'datefin':datefintostring
 ,'timefin':statusfin[i],'total':totalgroupe[i],'type_demande':typedemande[i],'user_id':window.localStorage.getItem("user_id"),'status':"en cours"}}).then(function(sucess)
 {
  console.log("datagroupe",sucess.data);
  if(sucess.data.message.type="info")
  {
  allsave++;
 if(allsave==taile){
    $ionicLoading.hide();
  var alertPopup=$ionicPopup.alert({
    title:"secussfull",
    template:"tout les demandes"
  })
  var url="http://localhost:8080/ionic_cake/groupedemandes/email.json";
   $http.post(url,{'GroupeDemande':{'demande_id':window.localStorage.getItem('demande_id_email')}}).then(function(success){
   console.log('ok',window.localStorage.getItem('demande_id_email'))
   },function(erreur){
 console.log('erreur')
   })
 $http.post("http://localhost:8080/ionic_cake/Tokens/sendNot").then(function(success){
   console.log('ok')
   },function(erreur){
 console.log('erreur')
   },function(Errreur){
     var alertPopup=$ionicPopup.alert({
       title:"erreur",
        tamplate:'connexion manquante'
     })
   })
          /*	$cordovaEmailComposer.isAvailable().then(function() {
  console.log('email ok');
 }, function () {
  console.log('email on');
 });
            $cordovaEmailComposer.open(email).then(null, function () {
   var alertPopup=$ionicPopup.alert({
    title:"email",
    template:"sended email"
  })
 
 }); */  
  $state.go('app.home');
 }
  }
  else
  {
    var url="http://localhost:8080/ionic_cake/groupedemandes/multidelete.json";
    $http.post(url,{'GroupeDemande':{'demande_id':sucess.data.message.damende_id}}).then(function(success){
      if(success.data.message.type="info")
      {
 $ionicLoading.hide();
    var alertPopup=$ionicPopup.alert({
      title:"erreur",
      template:"demande numero"+i+"est reject"
    })
      }
      else{
     
      }
    })
  
  }
 })
  //	console.log("year",year)
  //	console.log("month",month)
  //	console.log("day",day)
  }
    }
    else{
      var alertPopup=$ionicPopup.alert({
        title:"erreur",
        template:"first step non complet"
      })
    }
  },function(Erreur){
    var alertPopup=$ionicPopup.alert({
      title:"erreur",
      template:"<h2 style=color:red>problem dans la connetion"
    });
 $state.go('app.browse')
  });
}, 500);
 
 
 
}

 
}
}).controller('NoteCrtl',function($http,$state,$ionicLoading,$ionicPopup,$scope){
  
 /* var Url="http://192.168.1.25:8080/ionic_cake/Typenotes/index.json"
	$http.post(Url).then(function(success){
		console.log("les notes",success.data.typenote.notes)
		$scope.data = {
    model: "",
    availableOptions:success.data.typenote.notes
    
   };

  },function(erreur){
    $scope.data = {
      "model": "",
      "availableOptions":[
        {"Typenote":{"type":"avion","HT":1,"TAV":1}},{"Typenote":{"type":"restau","HT":1,"TAV":1}},{"Typenote":{"type":"train","HT":1,"TAV":1}}, {"Typenote":{"type":"tram","HT":0,"TAV":0}},{"Typenote":{"type":"TAXI","HT":0,"TAV":0}}
        ]
      
     };
     console.log("les notes",$scope.data)
  })*/
  $scope.data = {
    "model": "",
    "availableOptions":[
      {"Typenote":{'id':1,"type":"avion","HT":1,"TAV":1}},{"Typenote":{'id':2,"type":"restau","HT":1,"TAV":1}},
      {"Typenote":{'id':3,"type":"train","HT":1,"TAV":1}}, {'id':4,"Typenote":{'id':5,"type":"tram","HT":0,"TAV":0}},{"Typenote":{'id':6,"type":"TAXI","HT":0,"TAV":0}}
      ]
    
   };
   $scope.device="Euro"
   $scope.taux=1
  $scope.etap1=false;
  $scope.etap2=true;
  $scope.etap3=true;
  $scope.etap=function(a)
  {
    if(a==1)
    {
     
      if($scope.valeurTVA||$scope.valeurHT)
      {  $scope.etap1=true;
        $scope.etap2=false;
        $scope.etap3=true;}
        else{
         $scope.etap1=true;
         $scope.etap2=false;
         $scope.etap3=true
        }
    }
    if(a==2)
    {
      $scope.etap1=true;
      $scope.etap2=true;
       $scope.etap3=false;
    }
  }
  $scope.text={comment:""};
  $scope.allselect=true;
  $scope.cardvalide=true;
  $scope.aftervalide=false;
  $scope.fenish=false;
  $scope.bvalide=true;
  $scope.aferajoute=true;
  $scope.TVAshow=false
  $scope.numero=1;
  $scope.valeurTVA=false;
  $scope.valeurHT=false;
  $scope.myFile={}
  $scope.money={};
  var haurtax=[]
  var typeNote=[];
  var montans=[];
  var debutnote=[];
  var finnote=[];
  var justi=[];
  $scope.av={montant:0,
  montiv:""
  };
  
  var tva=0;
  var ht=0;
  $scope.selectn=function(a)
  {
    var objet={}
    obej=JSON.parse(a)
  if($scope.data.model=="")
  {
    $scope.allselect=true;
    console.log("model",obej)
  }
  else
  {
  
    $scope.allselect=false;
    
    console.log("model",obej)
  if(obej.TAV==1)
    {
      $scope.valeurTVA=true;
    }
    else{
    $scope.valeurTVA=false;	
    }
  if(obej.HT==1)
    {
      $scope.valeurHT=true
    }
    else{
    $scope.valeurHT=false
    }
  }
  
  }
  var today=new Date();
  $scope.today = today.toISOString().split("T")[0];	
  $scope.change=function(a)
  {
  var nextdate=new Date(a);
    nextdate.setDate(nextdate.getDate()+2);
    $scope.nextdate=nextdate.toISOString().split("T")[0];
    console.log('dateselect:',a);
  }
  
  $scope.valide=function(a,b){
    var objet={}
    obej=JSON.parse($scope.data.model)
    typeNote.push(obej.type);
    haurtax.push($scope.money.montant);
    if($scope.valeurTVA&&$scope.valeurHT)
  {
    
    montans.push($scope.money.montant+$scope.money.montant*$scope.money.tva/100-$scope.av.montant);
  }
  else{
    if($scope.valeurTVA&&!$scope.valeurHT)
      {
        montans.push($scope.money.montant+$scope.money.montant*$scope.money.tva/100-$scope.av.montant)
      }
      else
        {
          if(!$scope.valeurTVA&&$scope.valeurHT)
            {
              montans.push($scope.money.montant+$scope.av.montant-$scope.money.ht)
            }
            else{
              montans.push($scope.money.montant+$scope.av.montant)
            }
        }
  }
  var debut=new Date(a)
  var fin=new Date(b)
  var file =$scope.myFile.file;
  
  console.log('file is');
  console.log(file);
  var fd = new FormData();
  fd.append('file', file);
  console.log("fin",fin)
  console.log("debut",debut)
  fin.setHours(15);
fin.setMinutes(00);
fin.setSeconds(00);
datedebuttostring=debut.toISOString().split("T")[0];
    debutnote.push(debut);
   
var test=new Array({'model':'Note',
'motif':$scope.av.montiv,
'date':datedebuttostring,
'nom_invite' :"",
'societe_invite':"",
'type_depense_id':obej.id,
'montant_ttc':$scope.money.montant})

    var obj={
      'model':'Note',
      'motif':$scope.av.montiv,
      'date':datedebuttostring,
      'nom_invite' :"",
      'societe_invite':"",
      'type_depense_id':obej.id,
      'montant_ttc':$scope.money.montant,
     }
    justi.push(obj);
    console.log('tabe11',typeNote)
     console.log('tabe22',montans);
     console.log('tab3',justi)
     
    $scope.cardvalide=false;
  $scope.bvalide=false;
  $scope.aftervalide=true;
  $scope.fenish=true;
  console.log('ajoute',$scope.aftervalide)
      console.log('send',$scope.fenish)
  }
  $scope.ajoute=function()
  {
    $scope.allselect=true;
  $scope.bvalide=true;
  $scope.aftervalide=false;
  $scope.fenish=false;
  $scope.data.model="";
  $scope.money.montant="";
  $scope.text.comment="";
  $scope.cardvalide=true;
  $scope.etap1=false;
  $scope.etap2=true;
  $scope.etap3=true;
  $scope.numero++;
  $scope.aferajoute=false;
  }
  $scope.sende=function()
  {
       //Math.round(new Date(datefin[1]).getTime()-new Date(datedebut[1]).getTime()/ (1000 * 3600 * 24))+1;
  
  
    var i;
    
    var total=0;
    var total_ht=0
    var lengh=montans.length;
  for(var i=0;i<lengh;i++)
  {
  total=total+montans[i];
  total_ht=total_ht+haurtax[i]
  
  }
 
  //$ionicHistory.clearCache();
//$ionicHistory.clearHistory();
  
  var	url="http://79.137.38.63/notes/add"
  //var url="http://localhost:8080/ionic_cake/fichiers/add.json"
  $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <br/> note sending...'
        })
        console.log("total:",total)
        console.log("user_id",window.localStorage.getItem("user_id"))
        console.log("just",JSON.stringify(justi))
  $http.post(url,{'Note':{ 'devise':$scope.av.montiv, 'taux_change':'1','sous_total':total,'avance':$scope.av.montant,'total_ttc':total_ht,
  'remarque_collaborateur':$scope.text.comment,'user_id':window.localStorage.getItem("user_id")},'Justificatif':justi}).then(function(success){
      
 console.log("date de la note",success.data)
      var calcule=lengh;
      $ionicLoading.hide();
      var alertPopup=$ionicPopup.alert({
        title:"bon",
        template:'la note est envoyer'
      })
      //link="http://192.168.1.25:8080/ionic_cake/groupesnotes/add.json"
     /* for(var i=0;i<lengh;i++)
     {
        $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <br/> note '+i+1+" sending.."
      })
      var debut=new Date(debutnote[i])
      var fin=new Date(finnote[i])
      console.log("fin",fin)
      console.log("debut",debut)
      fin.setHours(15);
  fin.setMinutes(00);
  fin.setSeconds(00);
    datedebuttostring=debut.toISOString().split("T")[0];
    
    datefintostring=fin.toISOString().split("T")[0];
    console.log("fin",datefintostring)
      console.log("debut",datedebuttostring)
      console.log('user',window.localStorage.getItem("user_id"));
      window.localStorage.setItem('note_id_email',success.data.message.note_id)
             $http.post(link,{'GroupesNote':{'note_id':success.data.message.note_id,'type_note':typeNote[i],'montent':montans[i],'user_id':window.localStorage.getItem("user_id"),'status':'encours','notedebut':datedebuttostring,'notefin':datefintostring,'commentaire':$scope.text.comment}}).then(
           function(success)
           {
                 if(success.data.message.type="info")
           {
                    calcule--;
            if(calcule==0)
      {
        $ionicLoading.hide();
         var alertPopup=$ionicPopup.alert({
               title:"good",
               template:'all note sending'
             })
               var url="http://192.168.1.25:8080/ionic_cake/groupesnotes/email.json";
     $http.post(url,{'GroupesNote':{'note_id':window.localStorage.getItem('note_id_email')}}).then(function(success){
     console.log('ok',window.localStorage.getItem('note_id_email'))
     },function(erreur){
  console.log('erreur')
     },function(Errreur){
       var alertPopup=$ionicPopup.alert({
         title:"erreur",
          tamplate:'missing connection'
       })
     })
       $http.post("http://192.168.1.25:8080/ionic_cake/Tokens/sendNot").then(function(success){
     console.log('ok')
     },function(erreur){
  console.log('erreur')
     },function(Errreur){
       var alertPopup=$ionicPopup.alert({
         title:"erreur",
          tamplate:'missing connection'
       })
     })
      }
           }
           else{
             $ionicLoading.hide();
             var alertPopup=$ionicPopup.alert({
               title:"erreur",
               template:'note'+i+'not sending'
             })
           }
           }
         )
      }*/
      
      
   
  
  },function(erreur){
    $ionicLoading.hide();
    var alertPopup=$ionicPopup.alert({
      title:"erreur",
      template:'try again'
    })
  })
    
  }

}).controller('editprofil',function($http,$state,$ionicLoading,$ionicPopup,$scope,$cordovaSQLite){
  $scope.model=null;
  
  var years=['2017','2016','2015','2014','2013','2012','2011','2010','2009','2008','2007','2006','2005','2004','2003','2002','2001','2000']
  $scope.annes=years
  console.log('annes',$scope.prof)
  $scope.prof={
    'name':window.localStorage.getItem("user_session"),
    'email':window.localStorage.getItem("user_email"),
    'emailp':window.localStorage.getItem("user_emalp"),
     'ville':window.localStorage.getItem("user_ville"),
     'date':window.localStorage.getItem("user_date"),
     'nom':window.localStorage.getItem("user_nom"),
     'prenom':window.localStorage.getItem("user_prenom"),
    // 'born':window.localStorage.getItem("user_ne"),
    //'civil': window.localStorage.getItem("civil"),
     'country':window.localStorage.getItem("user_pays"),
     'role':window.localStorage.getItem("user_role"),
     'entite':window.localStorage.getItem("user_entite"),
     'girl':window.localStorage.getItem("user_fille"),
     'adressc':window.localStorage.getItem("user_adressc"),
     'adress':window.localStorage.getItem("user_adress"),
     'post':window.localStorage.getItem("user_post"),
     'tel':window.localStorage.getItem("user_tel"),
     'natio':window.localStorage.getItem("user_natio")  
   }
   $scope.selectcountrie=true;
   $scope.selectrole=true;
   $scope.etap1=true;
   $scope.etap2=false;
   $scope.etap3=false;
   
   console.log($scope.prof)
$http.get("http://services.groupkt.com/country/get/all").then(function(success){
  $scope.data = {
    countrie: "",
    availableOptions:success.data.RestResponse.result,
    
   };
  
   console.log( $scope.data.availableOptions)
   var x
  /* for(x in success.data.RestResponse.result ){
   var query = "INSERT INTO people (firstname, lastname) VALUES (?,?)";
   $cordovaSQLite.execute(db, query, [x.name,x.alpha2_code]).then(function(res) {
       console.log("INSERT ID -> " + x.name+" "+x.alpha2_code);
   }, function (err) {
       console.error(err);
   });
   var query = "SELECT firstname, lastname FROM people WHERE lastname = ?";
   $cordovaSQLite.execute(db, query, ['AF']).then(function(res) {
       if(res.rows.length > 0) {
           console.log("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname+" sieze "+res.rows.item.length);
       } else {
           console.log("No results found");
       }
   }, function (err) {
       console.error(err);
   });
  }*/
});

$scope.selectc=function(a)
{
  var objet={}
  obej=JSON.parse(a)
if($scope.data.countrie=="")
{
  $scope.selectcountri=true;
  console.log("model",obej)
}
else
{

  $scope.selectcountrie=false;

}
};
$scope.selectr=function(a)
{
  if($scope.data.role=="")
  {
    $scope.selectrole=true;
   }
  else
  {
  
    $scope.selectrolet=false;
  
  }
}

$scope.etap=function(a)
{
  if(a==1)
  {
   
     $scope.etap1=false;
      $scope.etap2=true;
      $scope.etap3=false;}
     
  
  if(a==2)
  {
    $scope.etap1=false;
    $scope.etap2=false;
     $scope.etap3=true;
  }
}
$scope.suivant=function(a)
 {
  if(a==1)
  {
   
     $scope.etap1=true;
      $scope.etap2=false;
      $scope.etap3=false;}
     
  
  if(a==2)
  {
    $scope.etap1=false;
    $scope.etap2=true;
     $scope.etap3=false;
  }

 }
 $scope.langue=function(label){
   console.log("langue:",label)
 }

 /*$scope.optionList = [
  {
    label: 'Français',
    value: 'FR'
  },
  {
    label: 'English',
    value: 'En'
  },
  {
    label: 'Spanish',
    value: 'sp'
  },
  {
    label: 'Deutsch',
    value: 'Dt'
  },
  {
    label: 'Arabe',
    value: 'Ar'
  }
];*/
$scope.optionList = [
  {
      "name":"Arabic",
      "nativeName":"العربية"
  },
  {
      "name":"Catalan",
      "nativeName":"Català"
  },
  {
      "name":"Chinese",
      "nativeName":"中文 (Zhōngwén), 汉语, 漢語"
  },
  {
      "name":"Croatian",
      "nativeName":"hrvatski"
  },
  {
      "name":"Czech",
      "nativeName":"česky, čeština"
  },
  {
      "name":"Danish",
      "nativeName":"dansk"
  },
  {
      "name":"Dutch",
      "nativeName":"Nederlands, Vlaams"
  },
  {
      "name":"English",
      "nativeName":"English"
  },
  {
      "name":"French",
      "nativeName":"français"
  },
  
  {
      "name":"German",
      "nativeName":"Deutsch"
  },
  {
      "name":"Greek",
      "nativeName":"Ελληνικά"
  },
  {
      "name":"Hindi",
      "nativeName":"हिन्दी, हिंदी"
  },
  {
      "name":"Hungarian",
      "nativeName":"Magyar"
  },
  {
      "name":"Indonesian",
      "nativeName":"Bahasa Indonesia"
  },
  {
      "name":"Irish",
      "nativeName":"Gaeilge"
  },
  {
      "name":"Italian",
      "nativeName":"Italiano"
  },
  {
      "name":"Japanese",
      "nativeName":"日本語 (にほんご／にっぽんご)"
  },
  {
      "name":"Kongo",
      "nativeName":"KiKongo"
  },
  {
      "name":"Korean",
      "nativeName":"한국어 (韓國語), 조선말 (朝鮮語)"
  },
  {
      "name":"Kurdish",
      "nativeName":"Kurdî, كوردی‎"
  },
  {
      "name":"Latin",
      "nativeName":"latine, lingua latina"
  },
  {
      "name":"Luxembourgish",
      "nativeName":"Lëtzebuergesch"
  },
  {
      "name":"Norwegian",
      "nativeName":"Norsk"
  },
  {
      "name":"Persian",
      "nativeName":"فارسی"
  },
  {
      "name":"Polish",
      "nativeName":"polski"
  },
  {
      "name":"Pashto",
      "nativeName":"پښتو"
  },
  {
      "name":"Portuguese",
      "nativeName":"Português"
  },
  {
      "name":"Quechua",
      "nativeName":"Runa Simi, Kichwa"
  },
  {
      "name":"Romansh",
      "nativeName":"rumantsch grischun"
  },
  {
      "name":"Kirundi",
      "nativeName":"kiRundi"
  },
  {
      "name":"Romanian",
      "nativeName":"română"
  },
  {
      "name":"Russian",
      "nativeName":"русский язык"
  },
  {
      "name":"Sanskrit",
      "nativeName":"संस्कृतम्"
  },
  {
      "name":"Sardinian",
      "nativeName":"sardu"
  },
  {
      "name":"Sindhi",
      "nativeName":"सिन्धी, سنڌي، سندھی‎"
  },
  {
      "name":"Northern",
      "nativeName":"Davvisámegiella"
  },
  {
      "name":"Samoan",
      "nativeName":"gagana faa Samoa"
  },
  {
      "name":"Sango",
      "nativeName":"yângâ tî sängö"
  },
  {
      "name":"Serbian",
      "nativeName":"српски језик"
  },
  {
      "name":"Scottish",
      "nativeName":"Gàidhlig"
  },
{
      "name":"Shona",
      "nativeName":"chiShona"
  },
  {
      "name":"Sinhala, Sinhalese",
      "nativeName":"සිංහල"
  },
  {
      "name":"Slovak",
      "nativeName":"slovenčina"
  },
  {
      "name":"Slovene",
      "nativeName":"slovenščina"
  },
  {
      "name":"Somali",
      "nativeName":"Soomaaliga, af Soomaali"
  },
  {
      "name":"Southern Sotho",
      "nativeName":"Sesotho"
  },
  {
      "name":"Spanish",
      "nativeName":"español, castellano"
  },
  {
      "name":"Swedish",
      "nativeName":"svenska"
  },
  
  {
      "name":"Turkish",
      "nativeName":"Türkçe"
  },
  {
      "name":"Ukrainian",
      "nativeName":"українська"
  }
]
$scope.selectn=function(a){
 
  }

$scope.valide=function(nom,prenom,datenaissance,civil,paysnaisssance,role,email,emailp,girl,entite,adress,adressc,
code,ville,nemuro,nation,diplome,niveau,annedip,institue){

  console.log("langue:",document.getElementById('countryid').value)
  var file = $scope.myFile;
  //$ionicHistory.clearCache();
//$ionicHistory.clearHistory();
  var file = $scope.myFile.file;
           
           console.log('file is ' );
           console.log(file);
           
           var uploadUrl = "http://192.168.1.25:8080/ionic_cake/fichiers/add.json";
           var fd = new FormData();
           fd.append('file', file);

$http.post(uploadUrl,fd,{
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           }).
then(function(response){
console.log(response.data);
var id_fichier=2 //response.data.message.data;
console.log(id_fichier)

})
}
}).controller('consulCrtl',function($scope,$http,$ionicHistory,$ionicLoading,$ionicPopup,$location,$state){
  $scope.showErreur=false;
  $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <br/>Réception de données...'
        });
  $http.post("http://localhost:8080/ionic_cake/users/index.json").then(function(success){
    $ionicLoading.hide();
  
    console.log(JSON.stringify(success.data.Users.admins));
  //window.localStorage.setItem("admin",JSON.stringify(success.data.Users.admins));
    console.log('appclt:',JSON.stringify(success.data.Users));
    $scope.condidats=success.data.Users.condidats;
    $scope.admins=success.data.Users.admins;
    window.localStorage.setItem("admin",success.data.Users.admins)
    //$state.go('app.search');
  },function(Erreur)
  {
  $ionicLoading.hide();
  var alertPopup=$ionicPopup.alert({
    title:'erreur',
    template:'<h1>réessayer<h1>'
  })
  $scope.showErreur=true;
  
  //$state.go('app.home');
  
  })
  
  
  $scope.condidature=function(){
    
  }
  
  //console.log('admin:',locage.User);
     /* if(window.localStorage.getItem("user_id")==null)
    {
         $location.replace();
    $state.go("login");
    
  
  }*/
  
  //console.log('condiadte:',JSON.stringify($scope.condidate.User))  ;
  $scope.resultat=true;
  //$scope.admin={};
  //console.log('condidat',$scope.admin.User);
     $scope.profile=function(info)
     {
       console.log(info);
       //window.localStorage.setItem("profilename",info.firstname+" "+info.lastname);
       window.localStorage.setItem("profileusername",info.username);
       window.localStorage.setItem("profilecreation",info.created);
        window.localStorage.setItem("firstname",info.firstname);
         window.localStorage.setItem("lastname",info.lastname);
         window.localStorage.setItem("idprofile",info.id);
         window.localStorage.setItem("profilemodfied",info.modified);
         
       $state.go('app.profilselect');
  
     }
     
     
}).controller('profselectCrtl',function($scope,$http,$state,$cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet){
  $scope.info={
"nom":window.localStorage.getItem("firstname"),
"prenom":window.localStorage.getItem("lastname"),
  "email":window.localStorage.getItem("profileusername"),
  "date": window.localStorage.getItem("profilecreation"),
  "naissance":window.localStorage.getItem("profilemodfied"),
  "adress":window.localStorage.getItem("firstname"),
  "nation":window.localStorage.getItem("lastname")
  
  }
  
  
$scope.etapedite=function(a)
{
  if(a==1)
  {
  $state.go("app.editeprofil")
  }
  else
  {
    if(a==2)
    {
      $state.go("app.editeprofil") 
    }
  }
}
  
  
  
  /*var slideIndex = 1;
  showDivs(slideIndex);
  
  $scope.plusDivs=function(n){
    showDivs(slideIndex=slideIndex+n);
  }
  
  function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
  
    if (n > x.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    x[slideIndex-1].style.display = "block";  
  }*/
}).controller("CreationCrlt",function($scope,$http){
  $scope.myFile={}
	
        $scope.emprole=false;   
        if(window.localStorage.getItem("user_role")=="directeur")
        {
          $scope.roles=["manager","directeur","administrateur"]
        }
        else
        {
          if(window.localStorage.getItem("user_role")=="manager")
          {
            $scope.roles=["manager","administrateur"]
          }
          else
          {
            $scope.roles=["administrateur"]
          }
        }
 $scope.data = {
    model: null,
    
   };
  $scope.loginData = {};
	$scope.all=true;
	$scope.select=function()
	{
		if($scope.data.model=="")
		{
      $scope.all=true;
      $scope.emprole=false;
		}
		else
		{
				$scope.all=false;
	if(($scope.data.model=="1")){
  $scope.hid=true;
  $scope.emprole=false;
console.log($scope.data.model);}
	else{
 console.log($scope.data.model);
   $scope.hid=false;
  if($scope.data.model=="3")
  {
$scope.emprole=true
  }
  else
  {
    $scope.emprole=false;
  }
  }}}
	  $scope.loginData={};
    var file = $scope.file;
    $scope.doLogin=function(){
      
        var file = $scope.myFile;
          //$ionicHistory.clearCache();
        //$ionicHistory.clearHistory();
          var file = $scope.myFile.file;
                   
                   console.log('file is ' );
                   console.log(file);
                   
                   var uploadUrl = "http://192.168.1.25:8080/ionic_cake/fichiers/add.json";
                   var fd = new FormData();
                   fd.append('file', file);
        
        $http.post(uploadUrl,fd,{
                      transformRequest: angular.identity,
                      headers: {'Content-Type': undefined}
                   }).
        then(function(response){
        console.log(response.data);
        var id_fichier=2 //response.data.message.data;
        console.log(id_fichier)
        if($scope.data.model!="2")
        {
          console.log($scope.loginData)
                 
        if($scope.loginData.username!=null&&$scope.loginData.password!=null&&$scope.loginData.Fisrtname!=null&&$scope.loginData.Lastname!=null)
        {
          var role=1;
              
            
          $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <br/> data sending.'
          });
          NewAcountservice.signupUser($scope.loginData.username,$scope.loginData.password,$scope.loginData.Fisrtname,$scope.loginData.Lastname,role
        ,id_fichier).then(function(data){
    
     
      // File name onl
     
        if(data.message.type=='info'){
           // var names = $scope.account.fisrtnames+" "+$scope.account.lastnames;
            
    
            
             var alertPopup = $ionicPopup.alert({
          title: 'all save',
          template: 'GOOD.'
        
           });
        
          
            
          $state.go('app');
            
        }
         
            if(data.message.type=='error'){ 
                      
            var alertPopup = $ionicPopup.alert({
          title: 'data not save!',
          template: 'try again.'
        
           });}
        
        
      });}
        
        else{
          var alertPopup = $ionicPopup.alert({
          title: 'Erreur!',
          template: 'plaise insert all information.'
           });
        }
      }
      else
      {
        console.log($scope.loginData)
    var role=2;
        if($scope.loginData.username!=null&&$scope.loginData.Fisrtname!=null&&$scope.loginData.Lastname!=null)
        {
          
    
          $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <br/> data sending.'
          });
          NewAcountservice.signupUser($scope.loginData.username,$scope.loginData.password,$scope.loginData.Fisrtname,$scope.loginData.Lastname,role,id_fichier).then(function(data){
        
            console.log(JSON.stringify(data))
               
                   
                
      // File for Upload
      /*var targetPath = $scope.pathForImage($scope.image);
     
      // File name only
      var filename = $scope.image;;
     
     var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params :{'user_session': window.localStorage.getItem('user_id'),'user_id':data.message.user_id}
      };
     
      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
       $ionicLoading.hide();
       cordova.plugins.IsDebug.getIsDebug(function(isDebug) {
        console.log(JSON.stringify(result));
    }, function(err) {
        console.error(err);
    });
          $scope.showAlert('Success', 'Image upload finished.');
        
      });*/
         
              
     
     // The rest of the app comes in here
    
    
      // Destination URL
    
     
        if(data.message.type=='info'){
            var names = $scope.loginData.username+" "+$scope.loginData.Fisrtname;
            
            //window.localStorage.setItem("user_session", names);
          
    
             var alertPopup = $ionicPopup.alert({
          title: 'all save',
          template: 'GOOD.'
        
           });
          
          
            $state.go('app');
        }
         
            if(data.message.type=='error'){     
                
            var alertPopup = $ionicPopup.alert({
          title: 'data not save!',
          template: 'try again.'
        
           });}
        });
    
    
    
    }
        
        else{
          var alertPopup = $ionicPopup.alert({
          title: 'Errreur!',
          template: 'plaise insert all information.'
           });
        }
      }
           $ionicLoading.hide();
        }, function(errResponse){
           
        });
          
        };
        $scope.image = null;
        
         $scope.showAlert = function(title, msg) {
           var alertPopup = $ionicPopup.alert({
             title: title,
             template: msg
           });
         };
         $scope.loadImage = function() {
          var options = {
            title: 'Select Image Source',
            buttonLabels: ['Load from Library', 'Use Camera'],
            addCancelButtonWithLabel: 'Cancel',
            androidEnableCancelButton : true,
          };
          $cordovaActionSheet.show(options).then(function(btnIndex) {
            var type = null;
            if (btnIndex === 1) {
              type = Camera.PictureSourceType.PHOTOLIBRARY;
            } else if (btnIndex === 2) {
              type = Camera.PictureSourceType.CAMERA;
            }
            if (type !== null) {
              $scope.selectPicture(type);
            }
          });
        };
        $scope.selectPicture = function(sourceType) {
          var options = {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            saveToPhotoAlbum: false
          };
         
          $cordovaCamera.getPicture(options).then(function(imagePath) {
            // Grab the file name of the photo in the temporary directory
            var currentName = imagePath.replace(/^.*[\\\/]/, '');
         
            //Create a new name for the photo
            var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";
         
            // If you are trying to load image from the gallery on Android we need special treatment!
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
              window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }
         
                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                  }, function(error){
                    $scope.showAlert('Error', error.exception);
                  });
                };
              }
            );
            } else {
              var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
              // Move the file to permanent storage
              $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                $scope.image = newFileName;
              }, function(error){
                $scope.showAlert('Error', error.exception);
              });
            }
          },
          function(err){
            // Not always an error, maybe cancel was pressed...
          })
        };
        $scope.pathForImage = function(image) {
          if (image === null) {
            return '';
          } else {
            return cordova.file.dataDirectory + image;
          }
        };
        $scope.uploadImage = function() {
          // Destination URL
          var url = "http://localhost:8888/upload.php";
         
          // File for Upload
          var targetPath = $scope.pathForImage($scope.image);
         
          // File name only
          var filename = $scope.image;;
         
          var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'fileName': filename}
          };
         
          $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
            $scope.showAlert('Success', 'Image upload finished.');
          });
        }
        
}).controller('ConsuNoteCrtl',function($scope,$state){

  $scope.notes=[{"montant":500,"User":{
    "firstname":"soufiane",
    "lastname":"soufiane"
  },"Date":{
      "firstname":"2017-11-10",
    }}, {"montant":500,"Date":{
      "firstname":"2017-10-10",
    },
    "User":{
      "firstname":"soufiane",
      "lastname":"elmoudden"
    }},{"montant":500,"Date":{
      "firstname":"2017-11-1",
    },
    "User":{
      "firstname":"collaborateur",
      "lastname":"ok"
    }},{"montant":500,"Date":{
      "firstname":"2017-10-10",
    },
    "User":{
      "firstname":"tesla",
      "lastname":"team"
    }}, {"montant":100,"Date":{
      "firstname":"2017-11-20",
    },
    "User":{
      "firstname":"soufiane",
      "lastname":"soufiane"
    }},{"montant":500.85,"Date":{
      "firstname":"2017-11-20",
    },
    "User":{
      "firstname":"ok",
      "lastname":"ok"
    }},{"montant":5000,"Date":{
      "firstname":"2017-11"},
      "User":{
        "firstname":"tesla",
        "lastname":"team"
      }}, {"montant":100,"Date":{
        "firstname":"2017-11-20",
      },
      "User":{
        "firstname":"tesla",
        "lastname":"team"
      }},{"montant":100,"Date":{
        "firstname":"2017-11-22",
      },"User":{
        "firstname":"tesla",
        "lastname":"team"},"User":{
          "firstname":"collaborateur",
          "lastname":"ok"
        }},{"montant":500,"Date":{
          "firstname":"2017-10-10",
        },
        "User":{
          "firstname":"tesla",
          "lastname":"team"
        }}, {"montant":100,"Date":{
          "firstname":"2017-11-20",
        },
        "User":{
          "firstname":"soufiane",
          "lastname":"soufiane"
        }},{"montant":500.85,"Date":{
          "firstname":"2017-11-20",
        },
        "User":{
          "firstname":"ok",
          "lastname":"ok"
        }},{"montant":5000,"Date":{
          "firstname":"2017-11"},
          "User":{
            "firstname":"tesla",
            "lastname":"team"
          }}, {"montant":100,"Date":{
            "firstname":"2017-11-20",
          },
          "User":{
            "firstname":"tesla",
            "lastname":"team"
          }},{"montant":100,"Date":{
            "firstname":"2017-11-22",
          },"User":{
            "firstname":"tesla",
            "lastname":"team"}}
  ]
  $scope.noteconult=true;
  $scope.infoNote=function(note)
  { 
    $scope.lesnotes={"name":"soufiane","montant":500}
    $scope.noteconult=false;
    $scope.lesnotes=note;
    $state.go("app.profilenote");
    console.log("scope",$scope.lesnotes);
  }
  $scope.sort = function(keyname){
    $scope.sortKey = keyname;   //set the sortKey to the param passed
    $scope.reverse = !$scope.reverse; //if true make it false and vice versa
}
$scope.remarque=function(){
  $state.go("app.remarquenote");
}
$scope.editer=function()
{
  $state.go("app.editeNote")
}
}).controller('editeNoteCrtl',function(){

});
